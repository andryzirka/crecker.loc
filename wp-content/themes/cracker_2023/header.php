<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php  wp_title();  ?></title>
    <?php wp_head(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.ico" type="images/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon-114x114.png">
</head>

<body id="body" class="body">
<div class="wrapperBlock">
    <header>
        <div class="basket">
            <div class="basket_container container">
                <a class="basket_quantity" href="#" rel="nofollow">
                    <img class="basket_img" src="<?php echo get_template_directory_uri(); ?>/images/header/semen.png" width="25" height="26" alt="core" loading="eager">
                    <span id="basket_number" class="basket_number">3</span>
                </a>

                <div class="basket_total">Total: <span class="basket_currency" data-curr="euro">143 €</span></div>
                <button class="btn_basketbtn_basket" type="button">Details</button>
                <div class="basket_box hidden">
                    <button class="btn_close__basket" type="button"></button>

                    <div class="basket_table">
                        <div class="basket_table__head">
                            <div class="b_icon b_icon__soybean"></div>
                            <div class="b_icon b_icon__sesame"></div>
                            <div class="b_icon b_icon__wheat"></div>
                            <div class="b_icon b_icon__corn"></div>
                        </div>
                        <div class="basket_table__body">
                            <ul class="basket_table__list">
                                <li class="basket_table__item">
                                    <span class="b_i">4</span>
                                    <span class="b_i">15</span>
                                    <span class="b_i">61</span>
                                    <span class="b_i">20</span>
                                    <span class="b_size">1.50</span>
                                    <span class="b_price">81.50</span>
                                    <a class="icon_del" href="#"></a>
                                </li>
                                <li class="basket_table__item">
                                    <span class="b_i">14</span>
                                    <span class="b_i">19</span>
                                    <span class="b_i">51</span>
                                    <span class="b_i">16</span>
                                    <span class="b_size">0.50</span>
                                    <span class="b_price">28.50</span>
                                    <a class="icon_del" href="#"></a>
                                </li>
                                <li class="basket_table__item">
                                    <span class="b_i">5</span>
                                    <span class="b_i">14</span>
                                    <span class="b_i">65</span>
                                    <span class="b_i">16</span>
                                    <span class="b_size">0.66</span>
                                    <span class="b_price">33.00</span>
                                    <a class="icon_del" href="#"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="basket_table__total">
                            <p class="basket_table__price">Total:<span>143</span></p>
                            <a class="basket_table__link" href="#" rel="nofollow">Checkout</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="header_wrapper">
            <div class="header_container container">
                <a class="logo" href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/header/logo.png" width="140" height="59" loading="eager" alt="Logo">
                </a>

                <nav class="nav">
                    <ul class="nav_list">
                        <li class="nav_item current"><a href="#">Home</a></li>
                        <li class="nav_item"><a href="#">About Us</a></li>
                        <li class="nav_item"><a href="#">Contacts</a></li>
                        <li class="nav_item"><a href="#">Checkout</a></li>
                        <li class="nav_item"><a href="#">Account</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>



