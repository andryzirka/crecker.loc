<?php
get_header(); ?>

<main class="main">
        <section class="section section_1">
            <div class="container container_section_1">
                <div class="box">
                    <div class="box_2">
                        <h2 class="caption caption_section_1 ">mostly tastes with freshes</h2>
                        <button class="btn btn_box_2">Taste it</button>
                    </div>
                    <div class="box_1 decor_left">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet,</p>
                    </div>
                </div>

            </div>
        </section>
        <section class="section section_2">
            <div class="container container_section_2">
                <h2 class="caption caption_section_2">About Cracker</h2>
                <img class="section_2__decor" src="<?php echo get_template_directory_uri(); ?>/images/bg_sections/crecker-1.png" width="380" height="380" loading="lazy" alt="crecker">
                <div class="section_2__box">
                    <p>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, architecto beatae vitae dicta sunt explicabo. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>

                </div>
                <img class="title_homepage" width="155" height="54" loading="lazy" src="<?php echo get_template_directory_uri(); ?>/images/Cracker-signed.png" alt="Cracker">


                <h2 class="caption caption_constructor">Cracker constructor</h2>
                <form id="form" class="form" name="constructor">
                    <p class="form_constructor">current value: <span class="form_constructor__value" data-curr="euro">143 €</span></p>
                    <label class="label">
                        <img class="label_img" src="<?php echo get_template_directory_uri(); ?>/images/calculator_icons/soybean.png"  width="60" height="60" loading="lazy" alt="soybean">
                        <input class="range" type="range" value="10" step="1" min="0" max="100" name="soybean">
                        <span class="range_value">10</span>
                    </label>
                    <label class="label">
                        <img class="label_img" src="<?php echo get_template_directory_uri(); ?>/images/calculator_icons/sesame.png" width="60" height="60" loading="lazy" alt="sesame">
                        <input class="range" type="range" value="30" step="1" min="0" max="100" name="sesame">
                        <span class="range_value">30</span>
                    </label>
                    <label class="label">
                        <img class="label_img" src="<?php echo get_template_directory_uri(); ?>/images/calculator_icons/wheat.png" width="60" height="60" loading="lazy" alt="wheat">
                        <input class="range" type="range" value="30" step="1" min="0" max="100" name="wheat">
                        <span class="range_value">30</span>
                    </label>
                    <label class="label">
                        <img class="label_img" src="<?php echo get_template_directory_uri(); ?>/images/calculator_icons/corn.png"  width="60" height="60" loading="lazy" alt="corn">
                        <input class="range" type="range" value="30" step="1" min="0" max="100" name="corn">
                        <span class="range_value">30</span>
                    </label>
                    <div class="form_box">
                        <img class="label_img" src="<?php echo get_template_directory_uri(); ?>/images/calculator_icons/pacage.png" width="59" height="59" loading="lazy" alt="pacage">
                        <details class="form_pack">
                            <summary class="form_pack__btn">Choose your pack</summary>
                            <div class="pack_list">
                                <label class="pack_element">
                                    <input class="pack_element__radio" type="radio" name="size" checked value="small">
                                    <span class="pack_element__name">small pack</span>
                                </label>
                                <label class="pack_element">
                                    <input class="pack_element__radio" type="radio" name="size" value="medium">
                                    <span class="pack_element__name">medium pack</span>
                                </label>
                                <label class="pack_element">
                                    <input class="pack_element__radio" type="radio" name="size" value="large">
                                    <span class="pack_element__name">large pack</span>
                                </label>
                            </div>
                        </details>
                        <div class="buy_wrapper">
                            <label class="buy_label" for="input_checkbox"></label>
                            <input id="input_checkbox" class="input_checkbox__buy" type="checkbox">
                            <button class="btn_buy" type="button">Add to cart</button>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </main>

<?php get_footer();