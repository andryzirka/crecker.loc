<?php
add_theme_support('admin-bar', array('callback' => '__return_false'));

// add support for thumbnails
add_theme_support('post-thumbnails');

/// Remove libs
function my_deregister_scripts()
{
    /// Remove wp-embed
    wp_deregister_script('wp-embed');
}

add_action('wp_footer', 'my_deregister_scripts');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


function remove_jquery_migrate($scripts)
{
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        if ($script->deps) {
            $script->deps = array_diff($script->deps, array('jquery-migrate'));
        }
    }
}

add_action('wp_default_scripts', 'remove_jquery_migrate');

add_filter('the_generator', '__return_empty_string');

/// Remove version WP
function rem_wp_ver_css_js($src)
{
    if (strpos($src, 'ver='))
        $src = remove_query_arg('ver', $src);
    return $src;
}

add_filter('style_loader_src', 'rem_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'rem_wp_ver_css_js', 9999);

///////////


add_action('wp_enqueue_scripts', 'front_scripts');
function front_scripts()
{
    wp_enqueue_style('style-for-all', get_stylesheet_directory_uri() . '/css/main.min.css');
    wp_enqueue_style('homepage', get_stylesheet_directory_uri() . '/css/homepage.min.css');

    wp_enqueue_script('main-js',  get_template_directory_uri() .  '/js/common.min.js', array('jquery'), false, true);
}
