<footer class="footer">
    <div class="container footer_decor">
        <div class="footer_list">
            <div class="footer_column">
                <div class="footer_title">
                    <img class="footer_title__icon"
                         src="<?php echo get_template_directory_uri(); ?>/images/svg/fa-phone.svg" width="30"
                         height="30" loading="lazy" alt="phone">
                    <span class="footer_title__name">phone</span>
                </div>
                <a class="link_phone" href="tel:+439873456782">+43 ( 987 ) 345 - 6782</a>
            </div>
            <div class="footer_column">
                <div class="footer_title">
                    <img class="footer_title__icon"
                         src="<?php echo get_template_directory_uri(); ?>/images/svg/fa-map-marker.svg" width="20"
                         height="30" loading="lazy" alt="phone">
                    <span class="footer_title__name">Address</span>
                </div>
                <div class="footer_address">
                    Cracker Inc.
                    10 Cloverfield Lane
                    Berlin, IL 10928
                    Germany
                </div>
            </div>
            <div class="footer_column">
                <div class="footer_title">
                    <img class="footer_title__icon"
                         src="<?php echo get_template_directory_uri(); ?>/images/svg/fa-share-alt.svg" width="30"
                         height="30" loading="lazy" alt="phone">
                    <span class="footer_title__name">Share us</span>
                </div>
                <ul class="share_list">
                    <li class="share_item">
                        <img class="share_img"
                             src="<?php echo get_template_directory_uri(); ?>/images/soc/pinterest-icon.png" width="50"
                             height="50" alt="pinterest icon" loading="lazy">
                        <a href="#" class="share_name" rel="nofollow">https://www.pinterest.com/</a>
                    </li>
                    <li class="share_item">
                        <img class="share_img"
                             src="<?php echo get_template_directory_uri(); ?>/images/soc/facebook-icon.png" width="50"
                             height="50" alt="facebook icon" loading="lazy">
                        <a href="#" class="share_name" rel="nofollow">https://www.facebook.com/</a>
                    </li>
                    <li class="share_item">
                        <img class="share_img"
                             src="<?php echo get_template_directory_uri(); ?>/images/soc/google-icon.png" width="50"
                             height="50" alt="google icon" loading="lazy">
                        <a href="#" class="share_name" rel="nofollow">https://www.google.com/</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>

</html>

