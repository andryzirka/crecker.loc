<?php

// Подключение к базе данных
$host = "localhost";
$user = "root";
$password = "";
$database = "test_users";
$port = 3306;
$conn = mysqli_connect($host, $user, $password);


// Проверка соединения
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


// Проверка существования базы данных
$sql = "CREATE DATABASE IF NOT EXISTS $database";
if (mysqli_query($conn, $sql)) {
    echo "Database created successfully </br>";
} else {
    echo "Error creating database: " . mysqli_error($conn);
}

///////////////////
$conn = mysqli_connect($host, $user, $password, $database);

// Проверка существования таблицы
$table = "users";
$result = mysqli_query($conn, "SHOW TABLES LIKE '$table'");




if (mysqli_num_rows($result) == 0) {
    // Таблица не существует

    // Создание таблицы
    $sql_create_table = "CREATE TABLE $table (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        category VARCHAR(30) NOT NULL,
        firstname VARCHAR(50) NOT NULL,
        lastname VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL,
        gender VARCHAR(50) NOT NULL,
        birthDate DATE NOT NULL
    )";
    if(mysqli_query($conn, $sql_create_table)){
        echo "We created column users in database";
    } else{
        echo "Error creating table: ". mysqli_error($conn);
    }

} else {
    // Таблица существует
    echo "Table $table already exists";
}


$csv_file = fopen("dataset.csv", "r");
// Чтение строк из CSV файла и заполнение таблицы данными
while (($data = fgetcsv($csv_file, 1000, ",")) !== FALSE) {
    $category = mysqli_real_escape_string($conn, $data[0]);
    $firstname = mysqli_real_escape_string($conn, $data[1]);
    $lastname = mysqli_real_escape_string($conn, $data[2]);
    $email = mysqli_real_escape_string($conn, $data[3]);
    $gender = mysqli_real_escape_string($conn, $data[4]);
    $birthDate = date('Y-m-d', strtotime($data[5]));

    $sql_insert = "INSERT INTO users (category, firstname, lastname, email, gender, birthDate)
                   VALUES ('".$category."', '".$firstname."', '".$lastname."', '".$email."', '".$gender."', '".$birthDate."')";


    if(mysqli_query($conn, $sql_insert)){
        echo "Record inserted successfully";
    } else{
        echo "Error inserting record: ". mysqli_error($conn) . "</br>";
    }

}

// Закрытие файла CSV
fclose($csv_file);

// Закрытие соединения с базой данных
mysqli_close($conn);

?>