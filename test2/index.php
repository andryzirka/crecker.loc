<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test_users";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$perPage = 100;
$offset = ($page - 1) * $perPage;

$sql = "SELECT * FROM users LIMIT ?, ?";

$stmt = $conn->prepare($sql);
if (!$stmt) {
    die("Error: " . $conn->error);
}
$stmt->bind_param("ii", $offset, $perPage);
if (!$stmt->execute()) {
    die("Error: " . $stmt->error);
}
$result = $stmt->get_result();

echo "<table>";
    echo "<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Gender</th><th>Birth Date</th></tr>";
    while ($row = $result->fetch_assoc()) {
    echo "<tr>";
        echo "<td>" . $row["id"] . "</td>";
        echo "<td>" . $row["firstname"] . "</td>";
        echo "<td>" . $row["lastname"] . "</td>";
        echo "<td>" . $row["email"] . "</td>";
        echo "<td>" . $row["gender"] . "</td>";
        echo "<td>" . $row["birthdate"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";

$totalResults = $conn->query("SELECT COUNT(*) FROM users")->fetch_row()[0];

$totalPages = ceil($totalResults / $perPage);

echo "<div>";
    for ($i = 1; $i <= $totalPages; $i++) {
    if ($i == $page) {
    echo "<span>$i</span>";
    } else {
    echo "<a href=\"?page=$i\">$i</a>";
    }
    }
    echo "</div>";


